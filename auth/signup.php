<?php
require_once "app/init.php";
if(!empty($_POST)){
    $email = $_POST['email'];
    $username = $_POST['username'];
    $password = $_POST['password'];
    
    $validator = new Validator($database,$errorHandler);
    $validation = $validator->check($_POST,[
        'email' => [
            'required' => true,
            'maxlength' => 255,
            'unique' => 'users',
            'email' => true
        ],
        'username' => [
            'required' => true,
            'minlength' => 2,
            'maxlength' => 20,
            'unique' => 'users'
        ],
        'password' => [
            'required' => true,
            'minlength' => 8
        ]
    ]);
    if($validation->fails()){
//        displays the errors
//        echo "<pre>",print_r($validation->errors()->all()),"<pre>";
    }else{
//        create the user
        $created = $auth->create([
            'email'=>$email,
            'username'=>$username,
            'password' => $password
        ]);
        if($created){
            header('Location: index.php');
        }
    }
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Sign Up</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
</head>

<body>
   <div class="tp"></div>
    <!--
    <form action="signup.php" method="post">
        <fieldset>
            <legend>Sign Up</legend>
            <label>
                Email:
                <input type="text" name="email">
            </label>
            <?php
            if($validation->errors()->has('email')):
            ?>
            <p class="danger">
                <?= $validation->errors()->first('email'); ?>
            </p>
            <?php endif;?>
            <label>
                User Name:
                <input type="text" name="username">
            </label>
            <label>
                Password:
                <input type="password" name="password">
            </label>
            <input type="submit" value="Sign Up">
        </fieldset>
    </form>
-->
    <div class="container d-flex align-items-center">
        <form action="signup.php" method="POST" id="form">
            <legend class="text-center font-weight-bold heading">Sign Up</legend>

            <div class="form-group">
                <label class="mt-3">Email:</label>
                <input type="email" class="form-control" id="" placeholder="Enter the email:" name="email">
<?php
if($validation->errors()->has('email')):
?>
<!--<p class="danger">-->
    <?= $validation->errors()->first('email'); ?>
<!--</p>-->
<?php endif;?>
                <label class="mt-3">Username:</label>
                <input type="text" class="form-control" id="" placeholder="Enter the username:" name="username">
                <label class="mt-3">Password:</label>
                <input type="password" class="form-control" id="" placeholder="Enter the password:" name="password">
                
                <?php
                if($validation->fails())
//                   $error_array = $validation->errors()->all();
//                   print_r($error_array['password']);
                    echo "<pre>",print_r($validation->errors()->all()),"</pre>";
                ?>
            </div>



            <!--            <button type="submit" class="btn btn-primary mt-3">Submit</button>-->

            <button type="submit" class="btn btn-outline-info mt-3">Submit</button>

        </form>
    </div>
</body>

</html>
