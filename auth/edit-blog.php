<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Add New Blog</title>
    <!--    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">-->
    
</head>
<?php
require_once "app/init.php";
$user = $auth->getAuthSession();
if(!empty($_POST)){
    if(isset($_POST["submit"])){
        $img = $_POST["filesupport"];//If the image is not updated then bringing the previous image!!!
        if(!empty($_POST["image"])){
            $img = $_POST["image"];
        }
        $draft = 0;
        if(isset($_POST["isdraft"])){
            $draft = 1;
        }
//      When the user edits the post then the last modified date is saved in DateTime
        $current_date = date('Y-m-d H:i:s');
        $data = [
            "title"=>$_POST["title"],
            "description"=>$_POST["description"],
            "image"=>$img,
            "is_draft"=> $draft,
            "modified_at" => date('Y-m-d H:i:s', (strtotime($current_date)))
        ];
        
        $database->table("post_details")->update($data,"id","=",$_POST['postid']);
//        $database->table("post_details")->rawQueryExecutor("UPDATE tablenam set fiel = va;");
        
        header("Location: index.php");
    }else{
        $id = $_POST["editid"];
        $data = [
            "0" => ["author_id","=",$user],
            "1" => ["id","=",$id]
        ];
        $posts = $database->table("post_details")->andWhere($data)->get();
//        die(var_dump($posts));
?>
<body>
    <form action="edit-blog.php" method="post">
        <fieldset>
            <legend>Edit Your Blog</legend>
            Title: <input type="text" name="title" value="<?=$posts[0]->title?>">
            <br><br>
            Image: <input type="file" name="image" value="<?=$posts[0]->image?>">
            <input type="hidden" value="<?=$posts[0]->image?>" name="filesupport">
<!--            This input type hidden is for file-->
            <br><br>
            Content: <textarea name="description" id="" style="resize:none;" cols="50" rows=""><?=$posts[0]->description?></textarea>
            <br><br>
            
            <?php
//                die(var_dump("Triple"));
            ?>
            
            <?php
//                echo "Inside nothinf";
                if(($posts[0]->is_draft)==0){
//                    echo "Inside if";

            ?>
                Draft: <input type="checkbox" name="isdraft" readonly="readonly" disabled>
            <?php
                }else{
//                    echo "Inside else";
            ?>
                Draft: <input type="checkbox" name="isdraft" checked="true">
            <?php
                }
            ?>
            <br><br>
            <input type="hidden" name="postid" value="<?=$id?>">
            <input type="submit" value="Edit the post" name="submit">
        </fieldset>
    </form>
</body>

</html>
<?php
    }
}

?>