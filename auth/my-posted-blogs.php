<?php
require_once "app/init.php";

//var_dump($database->query("SELECT * FROM contacts"));
$auth->build(); //it automatically creates a table for me!!!
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Index</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.css">
    <style>
        .content::first-letter{
            font-size: 2rem;
        }
    </style>
</head>

<body>
<?php if($auth->check()): ?>
   <p class="text-center">You are signed in! <?= $auth->user()->username; ?>! <a href="signout.php">Sign Out</a></p>

    <div class="container mt-5 p-3 mb-2 bg-light text-dark shadow-lg animated fadeIn">
        <h1><u>My Posted Blogs</u></h1>
        <div class="float-right">
            <button type="button" class="btn btn-info"><a href="index.php" style="text-decoration:none;color:#fff;">View All Blogs</a></button>
            <button type="button" class="btn btn-info ml-4"><a href="add-blog.php" style="text-decoration:none;color:#fff;">Add New Blog</a></button>
        </div>
        <div class="clearfix mb-5"></div>
<?php
//    $user = $tokenHandler->getUserFromValidToken($_COOKIE["token"]);
    $user = $auth->getAuthSession();
//    $posts = $database->table("post_details")->where("author_id","=",$user)->get();
    $posts = $database->table("post_details")->sort_col_where("created_at","author_id","=",$user)->get();
    foreach($posts as $post){
?>
        <p style="margin-left:;font-size:1.2rem;" class="animated slideInDown"><strong><span class="text-info">Title:  </span><?= $post->title?></strong>
<?php
    if($post->is_draft == 0){
?>
    <strong class="float-right badge badge-pill badge-success" style="padding:10px;">Published</strong>
<?php
    }else{
?>
    <strong class="float-right badge badge-pill badge-success" style="padding:10px;">Draft</strong>
<?php
    }
?>
<!--
        <strong class="float-right bold text-success ml-3">Last modified: <?= $post->modified_at?></strong><strong class="float-right bold text-danger">Created at: <?= $post->created_at?></strong></p>
        <div class="clearfix"></div>
-->
        
        <p class="text-justify animated slideInUp content"><?= (str_replace("�","'",$post->description)); ?></p>
        <a href="" class="text-center mt-5" style="cursor:pointer;"><img src="images/<?=$post->image?>" alt=""></a>
        <div class="float-right mt-1 mb-1">
<!--            <a href="edit-blog.php?id=<?=$post->id?>" class="btn btn-info btn-sm mr-1"><i class="fa fa-pencil"> Edit</i></a>-->
        <form action="edit-blog.php" name="editform" method="post" style="display:inline-block;">
            <button class="btn btn-info btn-sm mr-1" type="submit" style="display:inline-block;"><i class='fa fa-pencil'></i> Edit</button>
<!--            <a href="" class="btn btn-info btn-sm mr-1"><i class="fa fa-pencil"> Edit</i></a>-->
            <input type="hidden" value="<?=$post->id?>" name="editid">
        </form>
            <a href="delete-blog.php?id=<?=$post->id?>" class="btn btn-danger btn-sm delete_btn mr-1"><i class="fa fa-trash"> Delete</i></a>
            <a href="delete-blog.php?id=<?=$post->id?>" class="btn btn-info btn-sm delete_btn"><i class="fa fa-archive"> Archive</i></a>
        </div>
        <div class="clearfix"></div>
        <hr>
<?php        
    }
?>
    
<?php else: ?>
<p>You are not signed in <a href="signin.php">Sign In</a> OR <a href="signup.php">Sign Up</a></p>
<?php endif;?>
    
    </div>

</body>

</html>
