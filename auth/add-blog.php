<?php
    require_once "app/init.php";
    if(!empty($_POST)){
//        $user = $tokenHandler->getUserFromValidToken($_COOKIE["token"]);
        $user = $auth->getAuthSession();
        //    var_dump($tokenNumber);
//        var_dump($user->user_id);
        if(isset($_POST["is_draft"])){
            $draft = 1;
        }else{
            $draft = 0;
        }
        $current_date = date('Y-m-d H:i:s');
        $data = [
            "author_id" => $user,
            "title" => $_POST["title"],
            "image" => $_POST["image"],
            "description" => $_POST["description"],
            "is_draft" => $draft,
            "created_at" => date('Y-m-d H:i:s', (strtotime($current_date))),
            "modified_at" => date('Y-m-d H:i:s', (strtotime($current_date))),
            "is_archive" => 0
        ];
//        die($database->table("post_details")->insert($data));
        $database->table("post_details")->insert($data);
        header("Location: my-posted-blogs.php");
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Add New Blog</title>
    <!--    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">-->
    
</head>

<body>
    <form action="add-blog.php" method="post">
        <fieldset>
            <legend>Add New Blog</legend>
            Title: <input type="text" name="title">
            <br><br>
            Image: <input type="file" name="image">
            <br><br>
            Content: <textarea name="description" id="" style="resize:none;" cols="50" rows=""></textarea>
            <br><br>
            Draft: <input type="checkbox" name="is_draft">
            <br><br>

            <input type="submit" value="Add the post">
        </fieldset>
    </form>
</body>

</html>
