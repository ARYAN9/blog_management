<?php
require "app/init.php";

if(!empty($_POST)){
    $email = $_POST['email'];
    $user = $userHelper->getUserByMail($email);
    if($user){
        $token = $tokenHandler->createForgotPasswordToken($user->id);
//        die(var_dump($token));
        if($token){
            $mail->addAddress($user->email);
            $mail->Subject = "Reset Password!";
            $mail->Body = "Use the below link within 15 minutes to reset your password. <br> <a href = 'http://localhost:8080/reset-password.php?token={$token}&email={$email}'>RESET PASSWORD</a>";
            if($mail->send()){
                echo "Password Resetb Link Has been sent!";
            }else{
                echo $mail->ErrorInfo;
            }
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Forgot Password</title>
</head>
<body>
    <h1>Forgot Password!</h1>
    
    <form action="forgot-password.php" method="post">
        <fieldset>
            <legend>Forgot Password</legend>
            <label>
                Email:
                <input type="text" name="email">
            </label>
            <input type="Submit" name="submit" value="Forgot Password">
        </fieldset>
    </form>
</body>
</html>