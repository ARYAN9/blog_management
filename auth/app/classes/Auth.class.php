<?php
class Auth{
    protected $database;
    
    protected $hash;
    protected $table = "author_details";
    protected $authSession = "user";
    
    protected $oldData = array();
    
    public function __construct(Database $database,Hash $hash){
        $this->database = $database;
        $this->hash = $hash;
    }
    
    public function build(){
        return $this->database->query("CREATE TABLE IF NOT EXISTS {$this->table}(id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT, email VARCHAR(255) NOT NULL UNIQUE, username VARCHAR(20) NOT NULL UNIQUE, password VARCHAR(255) NOT NULL)");
    }
    
    public function create($data){
        if(isset($data['password'])){
            $data['password'] = $this->hash->make($data['password']);
        }
        return $this->database->table($this->table)->insert($data);
    }
    public function signout(){
        setCookie('token','',time()-5000);
        $user_id = $_SESSION[$this->authSession];
        
        $sql = "DELETE FROM tokens WHERE user_id = {$user_id} and is_remember=1";
        $this->database->query($sql);
        //die($user_id);
        
        unset($_SESSION[$this->authSession]);
    }
    
    public function signIn($data){   
//        die(print_r($data));
        $user = $this->database->table($this->table)->where('username','=',$data['username']);
//        die(print_r($user));
        if($user->count() == 1){
            $user = $user->first();
            
            if($this->hash->verify($data['password'],$user->password)){
                $this->setAuthSession($user->id);
                return true;
            }
        }
        $this->oldData['username'] = $data['username'];
        $this->oldData['password'] = $data['password'];
        

        return false;
    }
    public function setAuthSession($id){
        $_SESSION[$this->authSession] = $id;
    }

    public function getAuthSession(){
        return $_SESSION[$this->authSession];
    }
    
    public function check(){
//        echo "Inside Jalsa";
        return isset($_SESSION[$this->authSession]);
    }
//    public function signOut(){
//        unset($_SESSION[$this->authSession]);
//    }
    public function getOldData(){
        return $this->oldData;
    }
    
    public function resetUserPassword(string $token,string $password){        
        $password = $this->hash->make($password);
        return $this->database->query("UPDATE author_details,tokens SET author_details.password = '$password',tokens.expires_at = NOW() WHERE author_details.id = tokens.user_id and tokens.token = '$token'");
    }
//    Returns false if the user is not logged in else user obj
    public function user(){
        if(!$this->check()){
            return false;
        }
        $user = $this->database->table($this->table)->where('id','=',$_SESSION[$this->authSession])->first();
        return $user;
    }
}
?>