<?php
class Database{
    protected $host = "localhost";
    protected $db = "blog_mgmt";
    protected $username = "aryan";
    protected $password = "aryan";
    
    protected $table;
    protected $stmt;
    protected $pdo;
    
    protected $debug = true;
//        This should always come from the .ini file not from the there so once chjange then at all 6the changed will happen
    
    public function __construct(){
        try{
            $this->pdo = new PDO("mysql:host={$this->host};dbname={$this->db}", $this->username, $this->password);
//            if($this->debug){
//                $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
//            }
        }catch(PDOException $e){
            die($this->debug ? $e->getMessage() : "Some Error While Connecting!");
        }
    }
    
    public function query($sql){
        return $this->pdo->query($sql); //executes the ddl statements
        //return $stmt->fetchAll();
    }
    
    public function table($tablename){
        $this->table = $tablename;
        return $this;
    }
    
    public function rawQueryExecutor($sql){
//        die(var_dump($sql));
        return $this->pdo->query($sql);
    }
    
    public function update($data,$field, $operator, $value){
        $condition = "";
        foreach($data as $key=>$data_value){
            $condition .= "`".$key."`" . "=" . "'".$data_value ."'". ",";
        }
        $condition = substr($condition,0,strlen($condition)-1);//for removing the last comma!!!
        $sql = "UPDATE `{$this->table}` SET $condition WHERE {$field} {$operator} :value";
        $this->stmt = $this->pdo->prepare($sql);
        return $this->stmt->execute(["value" => $value]);
    }
    
    public function delete($field, $operator, $value){
        $value1 = ':'.$value;
        $sql = "DELETE FROM `{$this->table}` WHERE $field $operator $value1";
//        die($sql);
        $this->stmt = $this->pdo->prepare($sql);
//        die($this->stmt->execute([$value1 => $value]));
        return $this->stmt->execute([$value1 => $value]);
    }
    
    public function sort_col_where($column_name,$field, $operator, $value){
        $this->stmt = $this->pdo->prepare("SELECT * FROM {$this->table} WHERE {$field} {$operator} :value ORDER BY $column_name DESC");
        $this->stmt->execute(["value" => $value]);
        return $this;
    }
    
    public function insert($data){
        $keys = array_keys($data);
        
        $fields = "`".implode("`, `", $keys)."`";
        $placeholder = ":".implode(", :", $keys);
        $sql = "INSERT INTO `{$this->table}` ({$fields}) VALUES ({$placeholder})";
        $this->stmt = $this->pdo->prepare($sql);
        return $this->stmt->execute($data);
    }
    
    public function where($field, $operator, $value){
        $this->stmt = $this->pdo->prepare("SELECT * FROM {$this->table} WHERE {$field} {$operator} :value");
        $this->stmt->execute(["value" => $value]);
        return $this;
    }
    
    public function andWhere($data){
        $condition = $data[0][0] . $data[0][1] . ":value";
        $value = array();
        $value[":value"] = $data[0][2];
        //    die(var_dump((array_slice($data, 1))));
        foreach(array_slice($data, 1) as $key=>$tp){
            $condition .= " and ";
            $condition .= $tp[0] . $tp[1] . ":value".$key;
            $temp_str = ":value".$key; 
            $value[$temp_str] = $tp[2];
        }
//        die(var_dump($condition));
        $this->stmt = $this->pdo->prepare("SELECT * FROM {$this->table} WHERE $condition");
        $this->stmt->execute($value);
        return $this;
    }
    
    public function exists($data){
        $field = array_keys($data)[0];
        return $this->where($field, "=", $data[$field])->count() ? true : false;
    }
    
    public function count(){
        return $this->stmt->rowCount();
    }
    
    public function get(){
        return $this->stmt->fetchAll(PDO::FETCH_OBJ);
    }
    
    public function first(){
//        die(print_r($this->get()[0]));
        return ($this->get()[0]);
    }  
}
?>
