<?php
class UserHelper{
    private $database;
    private $table = "author_details";
    
    public function __construct(Database $database){
        $this->database = $database;
    }
    
    public function getUserByMail(string $email){
        return $this->database->table($this->table)->where('email','=',$email)->first();
    }
    public function getUserByUsername(string $username){
        return $this->database->table($this->table)->where('username','=',$username)->first();
    }
}

