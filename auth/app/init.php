<?php
session_start();

$app = __DIR__;
require_once "{$app}/classes/Hash.class.php";
require_once "{$app}/classes/ErrorHandler.class.php";
require_once "{$app}/classes/Validator.class.php";
require_once "{$app}/classes/Database.class.php";
require_once "{$app}/classes/Auth.class.php";
require_once "{$app}/classes/TokenHandler.class.php";
require_once "{$app}/classes/UserHelper.class.php";
require_once "{$app}/classes/MailConfigHelper.class.php";


$hash = new Hash();
$database = new Database();
$auth = new Auth($database,$hash);
$errorHandler = new ErrorHandler();
$validation = new Validator($database,$errorHandler);
$tokenHandler = new TokenHandler($database,$hash);
$tokenHandler->build();
$userHelper = new UserHelper($database);
$mail = MailConfigHelper::getMailer(); 



//$auth->build();
if(isset($_COOKIE['token']) && $tokenHandler->isValid($_COOKIE['token'],1)){
    $token = $_COOKIE['token'];
//    I want the user or user_id
    $user = $tokenHandler->getUserFromValidToken($token);
//    die(var_dump($user))
    $auth->setAuthSession($user->user_id);
}
?>